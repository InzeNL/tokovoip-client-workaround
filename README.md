# TokoVoIP workaround script

**Download: https://gitlab.com/citRaTTV/tokovoip-client-workaround/-/archive/master/tokovoip-client-workaround-master.zip**

After downloading this fix, simply run `workaround.bat` ***as Administrator*** to apply the TokoVoIP fix to your system. To run as Administrator, right click on the file and click `Run as administrator` (Windows 10).

If the fix does not work, please ensure that you disable any anti-virus software that you may be running, and then re-run `workaround.bat` as specified above. Once you have run the script, you may re-enable your anti-virus software.
